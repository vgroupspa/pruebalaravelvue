<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Models\User;

class LoginController extends Controller
{
    /**
     * Login user and return a token
     */
    public function login(Request $request){
        //dd($request->all());
        $reglas = array(
			
			"email" => "required|max:255",
			"password" => "required|max:255",
			
		);

		$msgValidacion = array(
			"required" => "El campo es requerido",
			"numeric" => "El campo sólo admite caracteres numéricos",
			"max" => "El campo admite como máximo :max caracteres"
		);

		$validador = Validator::make($request->all(), $reglas, $msgValidacion);
        //dd($validador->errors());


		if(!$token = auth()->attempt([ 'email' => $request->email,'password' => $request->password])){
			return response()
			->json([
				"msg" => "error",
			]);
		}else{
			return response()
			->json([
				"msg" => "autenticacionOk",
				"info" => "¡Ingreso correcto!",
				"access" => "Bearer ".$token
			]);
		}
		
    }

}
