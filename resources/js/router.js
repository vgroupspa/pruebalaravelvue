import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)



function prueba(to, from, next){
	next();
}


export default new Router({
    //evita que salgan # en la url
    mode: 'history',
    
	routes:[
		{
			path: '/',//url
			name: 'login',//nombre ruta
			meta: { layout: 'base-login' },// layout base para el login 
			component: () => import('./views/login/Login'),//componente que cargara
			
			
		},
		{
			path: '/index',
			name: 'index',
			meta: { layout: 'base-dashboard' },
			component: () => import('./views/dashboard/Dashboard'),
			prueba,
			
		},
		
	],

});
